#!/bin/bash

# Find out where we're running from
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ ! "$(pwd)" == "${script_dir}" ]; then
  echo "Not allowed. First execute:"
  echo "cd ${script_dir}"
  echo " Then try again."
  exit 1
fi

options=()

# shellcheck disable=SC1090
reposettings="${script_dir}/repo_config.txt"
# shellcheck disable=SC1090
source "${reposettings}"

# read parameters from local .config file
# shellcheck disable=SC2154
usersettings="${HOME}/.config/docker/${container}-config.txt"
if [ -f "${usersettings}" ]; then
  # shellcheck disable=SC1090
  source "${usersettings}"
else
  echo "No custom settings file (${usersettings}) found. Using defaults."
fi

# shellcheck disable=SC2154
# create the data-store now,if it doesn't already exist.
if [ ! -d "${store}" ]; then
  echo "Creating storage location: ${store}"
  mkdir -p "${store}"
fi
options+=("-v ${store}:/srv/syslog-ng/")

# double quoting (SC2086) will cause errors with docker run. Therefore:
echo "Starting the container with these options:"
# shellcheck disable=SC2086
echo ${options[*]}
echo

# double quoting (SC2086) will cause errors with docker run. Therefore:
echo "Starting the container with these options:"
# shellcheck disable=SC2086
echo ${options[*]}
echo
# double quoting (SC2086) will cause errors with docker run. Therefore:
# shellcheck disable=SC2086 disable=SC2154
docker run ${options[*]} "${image}" || exit 1

# Always run with the latest updates.
distro=$(<"${HOME}/.config/docker/${container}-distro.txt")

# shellcheck disable=SC2154
case "${distro}" in
  alpine)
    docker exec -it "${container}" apk update
    docker exec -it "${container}" apk upgrade
    ;;
  arch)
    docker exec -it "${container}" pacman -Syu --noconfirm
    ;;
  debian)
    docker exec -it "${container}" apt-get update
    docker exec -it "${container}" apt-get -y upgrade
    ;;
  *)
    echo "Unknown distro \[${distro}\]"
    exit 1
    ;;
esac

sleep 10
docker logs "${container}"
